use hound;

/// Read wave samples and collect max absolute max integer values
/// Pre-reduce samples to max integer values per sampled second.
pub struct WaveReader {
}

impl WaveReader {
    pub fn read(path: &str) -> Vec<i16> {
        let mut reader = hound::WavReader::open(path).unwrap();
        let sample_rate = reader.spec().sample_rate;
        let samples = reader.samples::<i16>();

        samples.enumerate().fold(vec![], |mut values, entry| {
            let (index, result) = entry;
            let new_sample = (index % sample_rate as usize) == 0;

            if let Ok(value) = result {
                let value_abs = value.abs();

                if new_sample {
                    values.push(value_abs);
                    return values;
                }

                if let Some(last_value) = values.last() {
                    if value_abs > *last_value {
                        values.pop();
                        values.push(value_abs);
                    }
                }
            } else if let Err(err) = result {
                panic!("{}", err);
            }

            values
        })
    }
}
