use std::env;
use super::cliparser::CliOpts;
use super::peakfilter::PeakFilter;
use super::times::{Ranger, Range};
use super::wavereader::WaveReader;

pub fn run() {
	let CliOpts { file, peak_db } = CliOpts::build(env::args()).unwrap();

	let values = WaveReader::read(&file);
	let times = PeakFilter::filter(values, peak_db);
    let lead = 10;
    let trail = 10;
    let time_ranges = Ranger::range(times, lead, trail);

	for Range { from, to } in time_ranges {
        println!("{}-{}", from, to);
    }
}
