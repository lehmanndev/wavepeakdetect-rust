use std::cmp::max;

/// Timerange with from and to seconds 
#[derive(Debug)]
pub struct Range {
	pub from: usize,
	pub to: usize,
}

impl Range {
	pub fn new(from: usize, to: usize) -> Range {
		Range { from, to }
	}

	/// Test whether this range overlaps with an earlier range's to.
	pub fn overlaps(&self, earlier_range: &Self) -> bool {
		self.from <= earlier_range.to
	}
}

impl PartialEq for Range {
	fn eq(&self, other: &Self) -> bool {
		self.from == other.from && self.to == other.to
	}
}

/// Convert list of seconds into ranges with lead and trail seconds 
/// before/after the timestamp. If adjacent ranges overlap, they will 
/// be combined.
pub struct Ranger {
}

impl Ranger {
	pub fn range(times: Vec<usize>, lead: usize, trail: usize) -> Vec<Range> {
		let mut ranges: Vec<Range> = vec![];

		for t in times {
			let from: usize = max(0, t as i32 - lead as i32).try_into().unwrap();
			let range = Range::new(from, t + trail);

			if ranges.is_empty() {
				ranges.push(range);
				continue;
			}

			let mut prev_range = ranges.last_mut().unwrap();

			if range.overlaps(&prev_range) {
				(*prev_range).to = range.to;
			} else {
				ranges.push(range);
			}
		}

		ranges
	}
}

#[cfg(test)]
mod tests {
	use super::Ranger;
	use super::Range;

	#[test]
	pub fn non_overlapping() {
		let earlier = Range::new(0, 100);
		let now = Range::new(200, 300);
		assert!(!now.overlaps(&earlier));
	}

	#[test]
	pub fn overlapping_exactly() {
		let earlier = Range::new(0, 100);
		let now = Range::new(100, 200);
		assert!(now.overlaps(&earlier));
	}

	#[test]
	pub fn overlapping_with_difference() {
		let earlier = Range::new(0, 100);
		let now = Range::new(50, 200);
		assert!(now.overlaps(&earlier));
	}

	#[test]
	pub fn one_time() {
		assert_eq!(
			Ranger::range(vec![50], 50, 50),
			vec![Range::new(0, 100)],
		);
	}

	#[test]
	pub fn cap_first_from_to_zero() {
		assert_eq!(
			Ranger::range(vec![25], 50, 50),
			vec![Range::new(0, 75)],
		);
	}

	#[test]
	pub fn start_at_t_zero() {
		assert_eq!(
			Ranger::range(vec![0], 50, 50),
			vec![Range::new(0, 50)],
		);
	}

	#[test]
	pub fn non_overlapping_ranges() {
		assert_eq!(
			Ranger::range(vec![50, 200, 500], 50, 50),
			vec![Range::new(0, 100), Range::new(150, 250), Range::new(450, 550)],
		);
	}

	#[test]
	pub fn overlapping_ranges_at_the_beginning() {
		assert_eq!(
			Ranger::range(vec![50, 100, 300], 50, 50),
			vec![Range::new(0, 150), Range::new(250, 350)],
		);
	}
}