const ZERO_DB: f32 = -60.0;

/// Filter values for peak dB values and return seconds above that threshold
pub struct PeakFilter {
}

impl PeakFilter {
    pub fn filter(peak_values: Vec<i16>, peak_db: f32) -> Vec<usize> {
        peak_values.iter().enumerate().fold(vec![], |mut seconds, entry| {
            let (second, value_abs) = entry;
            let frac: f32 = (*value_abs as f32) / i16::MAX as f32;
            let mut db = ZERO_DB;

            if frac > 0.0 {
                db = 20f32 * frac.log10();
            }

            if db >= peak_db {
                seconds.push(second);
            }

            seconds
        })
    }
}

#[cfg(test)]
mod tests {
    use super::PeakFilter;

    // Decibel:
    //    dB = 20 * log10(value / 32767)
    // 
    // Inverse of the decibel calulation:
    //    value = 10^(dB / 20) * 32767

    #[test]
    pub fn filter_values_at_and_above_threshold() {
        assert_eq!(
            PeakFilter::filter(vec![0, 102, 103, 104, 32767], -50.0f32),
            vec![3, 4],
        );
    }

    #[test]
    pub fn filter_silence() {
        assert_eq!(
            PeakFilter::filter(vec![0, 0], -60.0f32),
            vec![0, 1],
        );
    }

    #[test]
    pub fn filter_loudness() {
        assert_eq!(
            PeakFilter::filter(vec![32767, 32767], 0.0f32),
            vec![0, 1],
        );
    }
}