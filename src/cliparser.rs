#[derive(Debug)]
pub struct CliOpts {
    pub file: String,
    pub peak_db: f32,
}

impl CliOpts {
    /// Parse command line args
    /// Usage: <file> <peak dB>
    pub fn build(mut args: impl Iterator<Item = String>) 
    -> Result<CliOpts, &'static str> {
        // Omit the binary name
        args.next();

        let file = match args.next() {
            Some(arg) => arg,
            None => return Err("No filename provided"),
        };

        let peak_db = match args.next() {
            Some(arg) => {
                if let Ok(num) = arg.parse::<f32>() {
                    num
                } else {
                    return Err("Peak dB value is not parsable as float")
                }
            },
            None => return Err("No peak dB value provided"),
        };

        Ok(CliOpts { file, peak_db })
    }
}

#[cfg(test)]
mod tests {
    use super::CliOpts;

    #[test]
    pub fn fail_with_no_filename() {
        let args: Vec<String> = vec![String::from("bin")];
        let result = CliOpts::build(args.iter().map(|e| e.clone()));

        match result {
            Err(err) => {
                assert_eq!(err, "No filename provided");
            },
            Ok(opts) => {
                assert!(false, "build() is supposed to to fail if no filename is provided");
            }
        }
    }

    #[test]
    pub fn fail_with_filename_but_no_peak_db() {
        let args: Vec<String> = vec![String::from("bin"), String::from("somefile")];
        let result = CliOpts::build(args.iter().map(|e| e.clone()));

        match result {
            Err(err) => {
                assert_eq!(err, "No peak dB value provided");
            },
            Ok(opts) => {
                assert!(false, "build() is supposed to to fail if no peak dB is provided");
            }
        }
    }

    #[test]
    pub fn build_with_filename_and_peak_db() {
        let args: Vec<String> = vec![String::from("bin"), String::from("somefile"), String::from("-47")];
        let opts = CliOpts::build(args.iter().map(|e| e.clone())).unwrap();
        assert_eq!(opts.file, "somefile");
        assert_eq!(opts.peak_db, -47.0f32);
    }
}
